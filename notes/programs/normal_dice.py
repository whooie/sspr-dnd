import numpy as np
import matplotlib.pyplot as pp
import lib.plotdefs as pd
import random

def NdK_dist(N, K, M=-1, T=500000, plot=False):
    X = list()
    for t in range(T):
        R = [int(1 + K*random.random()) for n in range(N)]
        for m in range(N-M):
            R.pop(R.index(min(R)))
        X.append(sum(R))
    mu = np.mean(X)
    sigma = np.std(X)
    x_data = list(range(N-M, (N-M)*K+1))
    y_data = list(map(lambda x: X.count(x)/T, x_data))
    x_norm = np.linspace(np.min(x_data), np.max(x_data), 1000)
    y_norm = np.exp(-(x_norm - mu)**2/(2*sigma**2))/np.sqrt(2*np.pi*sigma**2)

    if plot:
        fig, ax = pp.subplots()
        ax.plot(x_data, y_data, "-C0", label=f"{N}d{K}, pick {M}")
        ax.plot(x_data, y_data, ".k")
        ax.plot(x_gauss, y_gauss, "--C0")
        ax.legend()
        pd.grid(True, ax)
        ax.set_xlabel("Sum result for {N}d{K}, pick {M} roll")
        ax.set_ylabel("Probability")
    return x_data, y_data, x_norm, y_norm

N =500000

C = 0
fig, ax = pp.subplots()

for K in range(1, 11):
    X = list()
    for n in range(N):
        X.append(sum([int(1 + 6*random.random()) for k in range(K)]))
    mu = np.mean(X)
    sigma = np.std(X)
    
    x_data = list(range(K - 2, 6*K+1 + 2))
    y_data = list(map(lambda x: X.count(x)/N, x_data))
    
    x_plot = np.linspace(np.min(x_data), np.max(x_data), 1000)
    y_plot = np.exp(-(x_plot - mu)**2/(2*sigma**2))/np.sqrt(2*np.pi*sigma**2)

    _fig, _ax = pp.subplots()
    _ax.plot(x_data, y_data, f"-C{C}", label=f"{K=}")
    _ax.plot(x_plot, y_plot, f"--C{C}")
    _ax.legend()
    pd.grid(True, _ax)
    _ax.set_xlabel("Sum result for $K$d6 roll")
    _ax.set_ylabel("Probability")
    _ax.set_xlim(-2, 63)
    _ax.set_ylim(-0.01, 0.25)
    _fig.savefig(f"normal_dice_{K=}.png")

    ax.plot(x_data, y_data, f"-C{C}", label=f"{K=}")
    ax.plot(x_plot, y_plot, f"--C{C}")
    C += 1

ax.legend()
pd.grid(True, ax)
ax.set_xlabel("Sum result for $K$d6 roll")
ax.set_ylabel("Probability")
ax.set_xlim(-2, 63)
ax.set_ylim(-0.01, 0.25)
fig.savefig("normal_dice.png")
