# Prelude

If we wander, if we seem to stray, remember that the best stories seldom take
the straightest way.

In some ways, it began when I heard her singing. Her voice twinning, mixing with
my own. Her voice was like a portrait of her soul: wild as a fire, sharp as
shattered glass, sweet and clean as clover.

No. It began at the University. I went to learn magic of the sort they talk
about in stories. Magic like Taborlin the Great. I wanted to learn the name of
the wind. I wanted fire and lightning. I wanted answers to ten thousand
questions and access to their archives. But what I found at the University was
much different than a story, and I was much dismayed.

But I expect the true beginning lies in what led me to the University.
Unexpected fires at twilight. A man with eyes like ice at the bottom of a well.
The smell of blood and burning hair. The Chandrian. Yes. I supposed that is
where it all begins. This is, in many ways, a story about the Chandrian.

But I suppose I must go even further back than that. If this is to be something
resembling my book of seeds, I can spare the time. It will be worth it if I am
remembered, if not flatteringly, then at least with some small amount of
accuracy.

But what would my father say if he heard me telling a story this way? 'Begin at
the beginning.' Very well, if we are to have a telling, let's make it a proper
one.

In the beginning, as far as I know, the world was spun out of the nameless void
by Aleph, who gave everything a name. Or, depending on the version of the tale,
found the names all things already possessed.

I see you laugh. Very well, for simplicity's sake, let us assume I am the center
of creation. In doing this, let us pass of innumerable boring stories: the rise
and fall of empires, sagas of heroism, ballads of tragic love. Let us hurry
forward to the only tale of any real importance. Mine.
