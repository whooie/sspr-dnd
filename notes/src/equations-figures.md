# Equations and Figures

The following are physical concepts and equations that will be considered to
accurately describe the SSPR-D&D world. All equations are borrowed from various
real-world physics, but are of course idealized and simplified to ensure that
the most math a player will be required to use is simple algebra.

## Heat Transfer

The equation likely to be used most frequency by players gives how much heat one
can liberate from an object by way of a specified temperature change (or
conversely how much heat is requires to give a desired change in the object's
temperature):
$$
Q = mC \Delta T
$$
where $m$ is the mass of the object, $\Delta T$ is the change in temperature,
$Q$ is heat, and $C$ is a proportionality constant known as the "specific heat
capacity" with dimensions [energy][mass]^-1[temperature]^-1. $C$ will vary
between materials and phases, but the general trend for most solid materials is
that $C$ will increase with density. Specific heat capacities (SHC) for some
common materials (taken from [Wikipedia][heat-capacities]) are tabulated below.

| Material              | Phase     | SHC (J/g K) |
| :-------------------- | :-------- | ----------: |
| Air (273 K)           | gas       | 1.0035      |
| Air (room conditions) | gas       | 1.012       |
| Aluminum              | solid     | 0.897       |
| Ammonia               | liquid    | 4.700       |
| Animal tissue         | mixed     | 3.5         |
| Brick                 | solid     | 0.840       |
| Carbon dioxide        | gas       | 0.839       |
| Copper                | solid     | 0.385       |
| Diamond               | solid     | 0.5091      |
| Ethanol               | liquid    | 2.44        |
| Glass                 | solid     | 0.84        |
| Gold                  | solid     | 0.129       |
| Granite               | solid     | 0.790       |
| Helium                | gas       | 5.1932      |
| Hydrogen              | gas       | 14.30       |
| Iron                  | solid     | 0.412       |
| Lead                  | solid     | 0.129       |
| Magnesium             | solid     | 1.02        |
| Marble                | solid     | 0.880       |
| Mercury               | liquid    | 0.1395      |
| Methanol              | liquid    | 2.14        |
| Nitrogen              | gas       | 1.040       |
| Oxygen                | gas       | 0.918       |
| Paraffin wax          | solid     | 2.5         |
| Sand                  | solid     | 0.835       |
| Silver                | solid     | 0.233       |
| Sodium                | solid     | 0.1230      |
| Soil                  | solid     | 0.800       |
| Steel                 | solid     | 0.466       |
| Tin                   | solid     | 0.227       |
| Titanium              | solid     | 0.523       |
| Tungsten              | solid     | 0.134       |
| Water (steam)         | gas       | 2.080       |
| Water                 | liquid    | 4.1813      |
| Water (ice)           | solid     | 2.05        |
| Wood                  | solid     | 1.2--2.9    |

A sister equation to this one is used to calculate how much heat is released or
required in a phase transition. It follows the form
$$
Q = mL
$$
where $m$ and $Q$ are again mass and heat, and $L$ takes the place of $C$ as the
"specific latent heat" with dimensions [energy][mass]^-1. Note here the
important lack of a $\Delta T$ factor. This is because phase transitions are
considered to take place at a constant temperature, but still involve the input
or release of heat. The specific latent heats (SLH) of fusion (dealing with the
solid-liquid transition) and vaporization (with liquid-gas) along with melting
and boiling points of some common materials (also taken from
[Wikipedia][specific-heats]) are tabulated below.

| Material       | SLH of fusion (J/g) | Melting point (C) | SLH of vaporization (J/g) | Boiling point (C) |
| :------------- | ------------------: | ----------------: | ------------------------: | ----------------: |
| Ammonia        | 332.17              | -77.74            | 1369                      | -33.34            |
| Carbon dioxide | 184                 | -78               | 574                       | -57               |
| Ethanol        | 108                 | -114              | 855                       | 78.3              |
| Helium         | --                  | --                | 21                        | -268.93           |
| Hydrogen       | 58                  | -259              | 455                       | -253              |
| Lead           | 23.0                | 327.5             | 871                       | 1750              |
| Nitrogen       | 25.7                | -210              | 200                       | -196              |
| Oxygen         | 13.9                | -219              | 213                       | -183              |
| Water          | 334                 | 0                 | 2264.705                  | 100               |



[heat-capacities]: https://en.wikipedia.org/wiki/Table_of_specific_heat_capacities
[specific-heats]: https://en.wikipedia.org/wiki/Latent_heat#Table_of_specific_latent_heats
