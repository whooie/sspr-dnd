# Summary

[Everything Physically Reasoned](README.md)

- [Introduction](introduction.md)

- [Characters and Attributes](characters-attributes/README.md)

- [Magic](magic/README.md)

    - [Sympathy](magic/sympathy/README.md)

        - [The Three Laws of Sympathy](magic/sympathy/three-laws.md)

        - [Using Sympathy](magic/sympathy/using-sympathy.md)

        - [Gameification](magic/sympathy/gameification.md)

---

[Equations and Figures](equations-figures.md)
