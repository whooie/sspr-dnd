# Magic

You will get no magical spells from me. In this world, lightning will not shoot
from your fingertips without reducing them to charcoal. Such things clearly
violate conservation laws and hence have no place in this system. But since I
believe that role-playing games really aren't as fun without some kind of
lightning generating mechanism, I've decided to shamelessly lift and adapt
(well, mostly lift) the concept of sympathy from Patrick Rothfuss's amazing
fantasy series, *The Kingkiller Chronicles*. I think this choice is ideal in
part because sympathy is particularly suited for the sort of gameification in
terms of hard quantifiables that this project is based on, and in part because I
just like it a lot.
