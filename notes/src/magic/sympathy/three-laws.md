# The Three Laws of Sympathy

The core of sympathy lies in the transference and conversion of energy across
links. In an ideal world, a skilled sympathist could cleverly engineer his
bindings to achieve perfectly efficient transfers and conversions. But of
course, no real-world sympathetic link is perfect; it is very nearly impossible
to achieve perfect efficiency. The dynamics of sympathy -- how much and how well
energy can flow between two objects or forms -- is governed by three simple
laws, as well as the sympathist and the strength of his Alar.

## Conservation

The first law, Conservation, is perhaps quite obvious. It states that the total
energy in the universe must remain constant. In other words, energy cannot be
created or destroyed, only gained or lost.

## Correspondence

The second law, Correspondence, states that similarity enhances sympathy. That
is, the quality of a sympathetic link between two objects is proportional to how
similar they are. Imagine the link as a pipe facilitating the flow of energy: it
can carry energy from one point to another, as well as change shape to alter the
qualities of the energy being carried. A perfect link is a perfect pipe -- all
of the water from point A arrives at point B. Recall the two coins bound
kinetically together. With perfect transference, lifting one of them would feel
like lifting exactly two coins due to the law of conservation.

In practice, however, even a good link is not perfectly efficient. Even when
binding two (very similar) coins together, the pipe will have some small leaks
because, at the end of the day, the two coins cannot be exactly the same. With
an imperfect link, some energy is lost, and one ends up having to put in more
energy to accomplish the desired effect. In this case, lifting one of the coins
would feel like lifting *slightly more* than exactly two coin.

One can then imagine what would happen if, instead of two coins, two dissimilar
objects were bound together, such as a coin and a washer, or a coin and a piece
of chalk. Considering the law of correspondence, the quality of the links in
these scenarios should be worse than the link between the two coins. More energy
would be lost through the links, and hence more energy would need to be provided
to get the other objects to follow the coin. Thus lifting the coin bound to the
washer might feel like lifting a deck of cards, while lifting the coin bound to
the chalk might feel more like lifting a glass of water.

## Consanguinity

The third law, Consanguinity, states that a part may represent a whole in a
sympathetic link. Suppose you wanted to bring a pot of water to a boil. One way
to accomplish this would be to get a near-identical pot of water and form a
thermal sympathetic link between the two while lighting a fire under the second.
The two pots are quite similar, so the sympathetic link between them is quite
sound; by moving the heat from the pot over the fire to the other, you would
eventually transfer enough heat to bring it up to a boil.

But while the link is quite efficient, the process as a whole is not: it
requires you to obtain and fill a whole other pot of water to achieve optimal
rates of energy transfer! Taking advantage of the law of consanguinity, you
could instead take a small vial-full of the water in the first pot and link it
to the rest while adding heat to your vial. This sympathetic link would be as
good or better than the previous one (what could be more similar to the water in
the pot than a piece of the water itself?), and the overall process requires
less time and materials.

