# Using Sympathy

Using sympathy is, overall, fairly straightforward: using his Alar, a sympathist
may form a sympathetic link between two objects[^1] and funnel energy through
the link to accomplish some goal. But to walk through this process raises some
questions that require additional clarification, detailed here.

## Where does the energy come from?

By now, I hope it's plain that energy 

## Where does all the energy go?

Considering the three laws of sympathy, you may wonder, "wait -- if energy is
conserved and no real sympathetic link is 100% efficient, then where does the
extra energy go?" This is an important consideration, not only because it deals
with the fundamental laws of sympathy, but because it provides the basis for the
way in which sympathy will be gameified here. (This will be detailed in the next
section, [Gameification](./gameification.md).)

## What about multiple bindings?

---

[^1]: "Object" is defined in perhaps a somewhat loose sense as "a collection of
  matter within some finite volume."

