# Sympathy

Everything is, to some degree, similar to everything else. Qualities like shape
and composition can conceptually link any two given objects together. These
links are called "sympathetic" links; through them, the objects can physically
influence each other at the behest of a person, the sympathist.

To bring about the formation of a sympathetic link, the sympathist wields his
"Alar," also known as the "riding-crop belief." An Alar is a special way of
thinking, a belief held so strongly that it influences the real world. When a
sympathist uses his Alar to believe that two objects are the same with respect
to a certain physical parameter -- heat, motion, luminance, etc. -- it becomes
true. The two objects are then said to be sympathetically bound to each other,
and energy corresponding to the physical parameter is free to be shared and
moved between them at the will of the sympathist.

If a sympathist were to kinetically bind, say, one coin to another, then the
first coin's motion would influence the other's. The sympathist could then
transfer some of the first coin's kinetic energy to the other and cause it to
move in perfect correspondence with the first. A more skilled sympathist can
even convert between forms of energy. If his Alar is strong enough, such a
sympathist could conceivably bind the kinetic energy of the first coin to the
luminance of the second, such that moving the first would cause the second to
glow.

Sympathy will be a 
