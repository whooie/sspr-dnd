> The production of heat alone is not sufficient to give birth to the impelling
> power: it is necessary that there should also be cold; without it, the heat
> would be useless.
>
> -- Nicolas Léonard Sadi Carnot

# Introduction

Let's get this out of the way: I don't respect RNG. The use of RNG in any game
is ultimately a refusal to implement a more complex way to model some
interaction or event, and thereby a commitment to a less skillful gaming
experience. In the specific case of vanilla D&D, dice rolls are often used to
gloss over the particulars of many things. Aiming bows, sweet-talking guards,
looking around rooms -- scenarios like these are subject to roll of a d20. I
contend that this is ridiculous: if any amount of mastery over any kind of skill
is to be claimed, then skills cannot be governed by the uniform distribution of
a d20, whereby one's chance of encountering wild success or miserable failure is
an astounding **10%**.

![distributions](normal_dice.png)

I am
willing to concede that not every instance of RNG is bad. There exists a point
beyond which 

D&D has two important parts: adventure and role-playing. The adventure part
encompasses all of the mechanisms of the game -- stats, NPCs, combat, etc. --
while the role-playing part is, well, role-playing. It's the fluff that sits on
top. Personally, I don't really care about role-playing; large commitments to
role-playing are for art majors and turbo-nerds.

Everything Physically Reasoned (EPR) D&D is intended to focus much more on the
adventure side of D&D. It's meant to establish a clear system of physical laws
for the world in which the adventure takes place and, most importantly, make
them available and transparent to both players and their characters.
